package com.mohendra.springboot.controller;

import com.mohendra.springboot.model.Spitter;
import com.mohendra.springboot.model.Spittle;
import com.mohendra.springboot.service.ApiService;
import com.mohendra.springboot.service.ApiServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mohit on 5/17/18.
 */
@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private ApiService apiService;

    @RequestMapping(value = "/spitter/{username}", method = RequestMethod.GET)
    public Spitter getSpitter(@PathVariable String username){
        return apiService.getValueOfSpitter(username);
    }

    @RequestMapping(value = "/spittle/{id}", method = RequestMethod.GET)
    public Spittle getSpittle(@PathVariable int id){
        return apiService.getValueOfSpittle(id);
    }
}
