package com.mohendra.springboot.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by mohit on 5/17/18.
 */
@Getter
@Setter
public class Spittle {
    int id;
    String text;
    Spitter spitter;
    Date when;
}
