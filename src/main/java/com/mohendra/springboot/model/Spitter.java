package com.mohendra.springboot.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by mohit on 5/17/18.
 */
@Getter
@Setter
public class Spitter {
    private int id;
    private String username;
    private String password;
    private String fullName;
    private String email;
}
