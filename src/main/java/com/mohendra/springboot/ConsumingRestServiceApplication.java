package com.mohendra.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumingRestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumingRestServiceApplication.class, args);
	}
}
