package com.mohendra.springboot.service;

import com.mohendra.springboot.model.Spitter;
import com.mohendra.springboot.model.Spittle;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by mohit on 5/17/18.
 */

@Service
public class ApiServiceImpl implements ApiService {
    @Override
    public Spitter getValueOfSpitter(String username) {
    return new RestTemplate().getForObject(
            "http://localhost:8080/spitters/{username}", Spitter.class, username);
    }
    @Override
    public Spittle getValueOfSpittle(int id){
        return new RestTemplate().getForObject(
                "http://localhost:8080/spittles/{id}", Spittle.class, id
        );
    }
}
