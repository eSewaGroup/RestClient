package com.mohendra.springboot.service;

import com.mohendra.springboot.model.Spitter;
import com.mohendra.springboot.model.Spittle;

/**
 * Created by mohit on 5/17/18.
 */
public interface ApiService {
    Spitter getValueOfSpitter(String username);

    Spittle getValueOfSpittle(int id);
}
